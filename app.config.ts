export default defineAppConfig({
    ui: {
        primary: 'merlot',
        gray: 'cool',
        button: {
            rounded: 'rounded-sm',
            variant: {
                solid: 'shadow-none px-3.5 py-2.5 ring-0',
                outline: 'ring-0 px-3.5 py-2.5 h-10 shadow-none'
            }
        },
        input: {
            rounded: 'rounded-none',
        }
    }
})
