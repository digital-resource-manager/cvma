
export default function useFilterMenu()
{
    function Filter() {
      
    let continents=[];
    let countries=[];
    let provincestate=[];
    let cities=[];
    let objects=[];
    
    let inputFields = [
    {text: "Kontinent", id: continents},
    {text: "Land", id: countries},
    {text: "Bundesland", id: provincestate},
    {text: "Stadt", id: cities},
    {text: "Objekt", id: objects}
    ];
    return inputFields; 
    };
    return {
        Filter
        }

};