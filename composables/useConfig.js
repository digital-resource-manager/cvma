let conf= ref([])
const fetchConf = async () => {
	
	//conf = await $fetch("https://telotawebdev.bbaw.de/cvma/config/drm_config.json")
	//conf = await $fetch("http://localhost:3000/config/drm_config.json");
	conf = await $fetch("/config/drm_config.json");
};
fetchConf();
//import conf from "@/data/config/drm_config.json";
//import conf from "@/data/config/config-ikare_to-fylr.json";
//Konfiguration der Felder fuer die Tabellenansicht
export const useConf = () => {
	return {
		conf,
	};
};
